# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
#from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .serializers import StockSerializer
from .models import Stock

# Create your views here.
#class StockList(APIView):
#    def get(self, request):
#        serializer = StockSerializer(stocks, many=True)
#        return Response(serializer.data)

#    def post(self):
#        pass
class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer
